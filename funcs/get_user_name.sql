
delimiter $$ ;
create function get_user_name(user_id INT) returns varchar(100)
begin

return (
  select concat(first_name, ' ', last_name)
    from users
    where id = user_id
  )
;

end$$

