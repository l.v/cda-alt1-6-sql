
insert into activities(name) values("Apiculture");
insert into activities(name) values("Agriculture");
insert into activities(name) values("Permaculture");
insert into activities(name) values("Orticulture");
insert into activities(name) values("Charpenterie");
insert into activities(name) values("Massonerie");
insert into activities(name) values("Potterie");
insert into activities(name) values("Confiture");
insert into activities(name) values("Méchanique");
insert into activities(name) values("Pèche");
insert into activities(name) values("Cuisine");


-- console.log([
--   ["2020-01-01", "Martin", "Matin", "host"],
--   ["2020-01-01", "Martine", "Gélatine", "host"],
--   ["2020-01-01", "Corentin", "Terrain", "volunteer"],
--   ["2020-01-01", "Sébastien", "Et la marie morgane", "volunteer"],
--   ["2020-01-01", "Valentin", "Bourin", "volunteer"],
--   ["2020-01-01", "Aubertin", "Aubergine", "volunteer"],
-- ].reduce((acc, curr) => {
--   return acc += `(${curr.map(e => `"${e}"`)}),\n`
-- }, "insert into users(registration_date, first_name, last_name, type) values\n").replace(/,\n$/, ';'))

insert into users values
(1, "2020-01-01","Martin","Matin","host"),
(2, "2020-01-01","Martine","Gélatine","host"),
(3, "2020-01-01","Aubertin","Aubergine","host"),
(4, "2020-01-01","Robin","Le Parrain","host"),
(5, "2020-01-01","Cotentin","Vaccin","host"),
(11, "2020-01-01","Corentin","Terrain","volunteer"),
(12, "2020-01-01","Sébastien","Et la marie morgane","volunteer"),
(13, "2020-01-01","Valentin","Bourin","volunteer");


-- console.log([
-- 	["La boite à meuh", "Lorem Ipsum", 3, "221B", "Baker Street", "London", "England", 1],
-- 	["La ferme ta...", "C'est pas bientôt fini oui ?!!", 3, "221B", "Baker Street", "London", "England", 1],
-- 	["La claque à frommage", "Lorem Ipsum", 3, "221B", "Baker Street", "London", "England", 1],
-- 	["La fabrique à air chaud", "Vive la france", 3, "55", "Rue du Faubourg Saint-Honoré", "Paris", "France", 1],
-- 	["Bitcoin mining farm", "Les meilleures pièces cryptologique du marché !", 3, "55", "Faires Farm Road", "Charlotte", "North Carolina", 1],
-- ].reduce((acc, curr) => {
--   return acc += `(${curr.map(e => `"${e}"`)}),\n`
-- }, "insert into host_places(name, notes, capacity, nb, street_name, city_name, country, user_id) values\n").replace(/,\n$/, ';'))

insert into host_places(name, notes, capacity, nb, street_name, city_name, country, user_id) values
("La boite à meuh","Lorem Ipsum","3","221B","Baker Street","London","England",1),
("La ferme ta...","C'est pas bientôt fini oui ?!!","3","221B","Baker Street","London","England",2),
("La claque à fromage","Lorem Ipsum","3","221B","Baker Street","London","England",3),
("La fabrique à air chaud","Je crois aux forces de l'esprit","3","55","Rue du Faubourg Saint-Honoré","Paris","France",4),
("Bitcoin mining farm","Les meilleures pièces cryptologique du marché !","3","55","Faires Farm Road","Charlotte","North Carolina",5);

insert into activities_to_host_places values
(1, 1, 2),
(2, 2, 5),
(3, 3, 8),
(4, 4, 1);

insert into activity_applications values
(1, 11, 1, "pending", "2020-01-01", "2020-01-04", null),
(2, 12, 2, "accepted", "2020-01-01", "2020-01-04", null),
(3, 13, 1, "rejected", "2020-01-01", "2020-02-01", null),
(4, 13, 2, "accepted", "2020-01-01", "2021-01-01", true);


insert into temporary_activities values
(1, "Faire des crop circles", 1, 5, "2020-01-01", "2020-01-02"),
(2, "Refaire le toit de la grange", 2, 5, "2020-01-01", "2020-01-08"),
(3, "Faire des confitures", 1, 1, "2020-01-01", "2020-01-02"),
(4, "Vérifier la santé du bétail de l'usine électrique", 1, 2, "2020-01-01", "2020-01-03");

insert into temporary_activities_to_activities values
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 5),
(5, 3, 8),
(6, 4, 1);

insert into temporary_activities_applications values
(11, 1, "pending", null),
(12, 2, "accepted", null),
(13, 1, "rejected", null),
(13, 3, "accepted", true);


