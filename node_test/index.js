const mysql      = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : process.env.MYSQL_USER,
  password : process.env.MYSQL_PW,
  database : 'woofing'
});

const [, , userId, activityId, xpAmount] = process.argv;
console.log(userId);
 

connection.connect();
 
connection.query(
  'call experience_add_to_user (?, ?, ?)',
  [userId, activityId, xpAmount],
  console.log
);
 
connection.end();
