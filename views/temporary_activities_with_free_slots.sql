
create view temporary_activities_with_free_slots as
select
*,
(capacity - slots_taken) as slots_available
from (
  select
    ta.*,
    count(
      case
        when ta_appli.accepted != "rejected"
        then 1
        else null
      end
    ) as slots_taken
  from temporary_activities ta

  left join temporary_activities_applications ta_appli
    on ta_appli.temporary_activity_id = ta.id

  group by ta.id
) as ta
;

