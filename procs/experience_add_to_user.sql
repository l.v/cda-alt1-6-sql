delimiter $$ ;
create procedure experience_add_to_user (
  in user_id int,
  in activity_id int,
  in experience_amount int
)
begin

declare user_lvl int;
declare user_xp int;
declare xp_required_to_next_level int;

if not (select exists (
  select 1 from levels
  where user_id = user_id and activity_id = activity_id
)) then
    insert into levels
    (user_id, activity_id, xp, level)
    values
    (user_id, activity_id, get_xp_to_next_level(2), 1);
end if;


-- todo DRY
select xp into user_xp from levels
where user_id = user_id and activity_id = activity_id;
select level into user_lvl from levels
where user_id = user_id and activity_id = activity_id;


set xp_required_to_next_level = get_xp_to_next_level(user_lvl + 1);

-- todo: will get level + 1 even if the user gets enough xp to pass multiple levels at once
update levels
  set xp = user_xp + experience_amount,
      level = (
        case
          when user_xp + experience_amount >= xp_required_to_next_level
          then user_lvl + 1
          else user_lvl
        end
      )
  where user_id = user_id and activity_id = activity_id;

end$$

