delimiter $$ ;
create trigger tempactiv_give_xp_when_present
  after update
  on temporary_activities_applications
  for each row
begin

declare xp_amount int;
declare to_activity_id int;
declare finished_cur int default 0;
DECLARE cur_activity CURSOR FOR
  SELECT  activity_id
  FROM    temporary_activities_to_activities
  where   temporary_activities_id = new.temporary_activity_id
;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished_cur = 1;
OPEN cur_activity;

if ((old.was_here is null or old.was_here = 0) and new.was_here = 1) then
  select get_xp_for_duration(start_date, end_date)
    into xp_amount
    from temporary_activities
    where id = new.temporary_activity_id;

  add_xp: LOOP
    FETCH cur_activity INTO to_activity_id;
    IF finished_cur = 1 THEN 
      LEAVE add_xp;
    END IF;

    call experience_add_to_user(
      new.from_user_id,
      to_activity_id,
      xp_amount
    );
  END LOOP add_xp;
end if;

CLOSE cur_activity;

end$$
delimiter ;

